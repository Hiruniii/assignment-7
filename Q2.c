//Write a program to find the Frequency of a given character in a given string.
 #include <stdio.h>
 
 
int main()
{
  	char str[1000];
  	int i;
  	
  	int count[256] = {0}; 
 
  	printf("\n Please Enter any String :  ");
  	gets(str);
  	
  	
  	for(i = 0; str[i] != '\0'; i++)
  	{
  		count[str[i]]++;
	}
  		
  	for(i = 0; i < 256; i++)
  	{
		if(count[i] != 0)
		{
			printf("Character '%c' Occurs %d Times \n", i, count[i]);
		}
	}
	
  	return 0;
}
